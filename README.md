# DeepRare - codes coming soon

This is the git repository for the DeepRare Model.

DeepRare is a saliency model which uses the power of DNNs feature extraction and 
the genericity of feature-engineered algorithms. Our tests show that while the 
results are good on classical saliency benchmarks it is still very good on 
datasets containing odd objects where DNN-based models provide very poor results
even when re-trained.